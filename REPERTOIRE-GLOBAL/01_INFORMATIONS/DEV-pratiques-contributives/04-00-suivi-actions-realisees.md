## ACTIONS RÉALISÉES

*  (100%) merge de `develop` dans `master` avec conservation de l'historique `develop` (archive avant de décider de la détruire plus tard)

## ACTIONS EN COURS DE RÉALISATION

*  Rédaction du guide de contribution
*  Tenue de ce fichier actions-realisees.md