WIP (work in progress) - TEC (Travail en Cours)

## ANALYSE DES INFORMATIONS RÉCOLTÉES SUR LES WORKFLOW GIT
   
   Analyse des informations récoltées sur les workflow git - [voir le document recolte-informations-sur-les-workflow-git.md](https://framagit.org/materiel-libre/materiel-libre/blob/14-ecrire-le-fichier-guide-de-contribution-pour-le-workflow-de-ce-projet/pratiques-contributives/recolte-informations-sur-les-workflow-git.md)

### Principaux enseignements en retour des informations récoltées

1.  Pour que les utilisateurs de git en ligne de commande, qui ne sont habituées qu'à l'utilisation de la branche `master`, il est nécessaire de publier des choses dans la branche `master` assez régulièrement, et assez rapidement
2.  Les workflows recommandés, proposent d'aller vers des utilisations simplifiées: une branche `master` protégée, sur laquelle on pique des branches par élément développé (une branche = un élément développé), lesquelles, une fois prêtes, sont ensuite fusionnées dans la branche `master`
3.  Pour chaque développement d'élément, la proposition de gitlab consistant à démarrer par une issue pour chaque élément créé, permet un traçage intéressant, et une discussion au cours de la vie de cet élément, au travers de l'issue postée au départ. Cela permet aussi d'affecter des "étiquettes" d'avancée du projet grâce aux "label" et la possibilité d'ordonner les "labels".
4.  Les workflows avec des successions de branches et de sous-branches, deviennent difficiles à gérer, et ne sont pas plebiscités.
5.  Les 10 étapes du workflow de gitlab semblent permettre une progression tranquille et participative des développements
6.  Les outils proposés par l'interface web distante gitlab, ne sont pas tous accessibles en ligne de commande GIT sur une machine locale
7.  Alors que le jonglage entre les différentes branches est facilité lors de l'utilisation de l'interface distante gitlab, ce jonglage est beaucoup plus difficile pour les personnes utilisant exclusivement les lignes de commandes sur une machine locale. Travailler sur une branche distante (remote) en ligne de commande sur une machine locale, n'est vraiment pas aisé. 


### Principes de workflow que l'on pourrait retenir pour cette instance projet, au vue des informations récoltées

L'idée serait de choisir un workflow simple.

*   protéger la branche `master`, qui est la branche par défaut
*   toujours commencer en ouvrant une "issue" sous la forme d'une "proposition"
*   proposer de discuter l'issue, avant d'aller plus loin
*   depuis l'issue, créer une branche portant le nom de l'issue (automatique), sur la branche `master`
*   réaliser ce qui est prévu, en discutant etc ...
*   quand c'est prêt, proposer une fusion sur la branche `master` en plaçant les lettres `(WIP)` ou `[WIP]` au début du titre de la demande de fusion
*   discuter de la demande fusion à ce stade, et effectuer les modifications nécessaires si besoin
*   quand les modifications sont réalisées, fusionner dans `master`
*   quand les travaux correspondent à une version, créer une branche depuis `master` avec le nom de version 


#### Options "phases de gestion de projet"
Cette option permet de gérer les réalisations, par [phase de gestion de projet en utilisant l'outil de gitlab via les issues](https://about.gitlab.com/features/issueboard/). Ce principe permet de visualiser rapidement, en allant dans les issues listées (en choisissant issue board), les travaux en cours par phase de réalisation: en émergence, en cours de production, en cours de finalisation, etc ...
*   créer un phasage de projet via les `label` et les `listes de label`, incluant une phase `action directe`
*   étiquetter l'issue avec son `label` correspondant à sa phase projet, et faire évoluer ce `label` de phase projet au fur et à mesure de l'avancée


### Conséquences sur la façon dont gitlab est utilisé aujourd'hui sur cette instance projet (juillet 2018)

#### Rappel sur l'utilisation actuelle (Juillet 2018)
*   la branche master est protégée. Elle a été alimentée directement d'une structuration de répertoires, et de fichiers de configuration de gitlab dans le répertoire racine. Ces modifications de la branche `master` n'ont pas fait l'objet de discussion, ni fait l'objet d'issue.
*   une branche `developpement` permanente a été créée. C'est la branche par défaut. Cet ajout n'a fait l'objet d'aucune discussion, d'aucune issue.
*   des issues sont ouvertes selon l'idée *"always start with an issue"*, et pour chaque issue, une branche est ouverte portant le nom de l'issue. Chaque branche s'ouvre depuis la branche par défaut `developpement`.
*   aucune fusion sur la branche `master` n'a été réalisée.

#### Changements à apporter sur l'utilisation actuelle
*   "déplacer" les `branches issues` ouvertes sur la branche `developpement` sur la branche `master`
*   faire un examen des branches ouvertes et les mettre à jour selon le workflow proposé
*   écrire un guide de contribution, dans le fichier `contributing.md` et dans le wiki de l'instance, expliquant le workflow proposé aux personnes contributrices 


### Conséquence sur l'écriture du Guide de contribution

#### Écrire le guide de contribution en différenciant l'utilisation de l'interface distante de l'utilisation locale en ligne de commande 
Les outils proposés par l'interface web de framagit/gitlab, ne sont pas tous accessibles aux personnes qui n'utilisent que les lignes de commandes GIT sur leur ordinateur local.
Par exemple: le suivi des issues; l'accès facile aux différentes branches distantes et à leur contenu; etc ...
Il convient donc de faire un guide qui différencie les utilisations de l'interface distante framagit/gitlab, de l'utilisation exclusive en ligne de commandes GIT sur un poste local.