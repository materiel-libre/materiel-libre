## RÉCOLTE D'INFORMATIONS SUR LES WORKFLOW GIT EXISTANT (juillet 2018)

### Liste de principaux liens sur les workflow git, recommandés par les plateformes proposant git

*  [Le guide de workflow de GitLab](https://docs.gitlab.com/ee/workflow/gitlab_flow.html)
*  [Le GitFlow, d'après Vincent Driessen](https://nvie.com/posts/a-successful-git-branching-model/)
*  [Le GitHub Flow, d'après Scott Chacon](http://scottchacon.com/2011/08/31/github-flow.html)
*  [Le workflow GitHub simplifié publié sur le GitHub Guide](https://guides.github.com/introduction/flow/index.html)
*  [Le workflow simplifé de Atlasian sur le blog Atlasian](https://www.atlassian.com/blog/archives/simple-git-workflow-simple)
*  [Le tutoriel de Atlasian sur les différents scénarios de workflow git](https://www.atlassian.com/git/tutorials/comparing-workflows)
*  [Le guide de contribution de GitLab](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md)
*  [Le résumé du workflow complet en 10 étapes, conseillé par GitLab](https://about.gitlab.com/2016/10/25/gitlab-workflow-an-overview/)

Ces guides sont en langue anglaise.

À ces guides et articles, s'ajoutent des recommandations sur la façon d'utiliser git de façon collective de la part de GitLab

*  [Toujours commencer par une issue, pour éviter de finir comme un Gollum](https://about.gitlab.com/2016/03/03/start-with-an-issue/)
*  [Placer des étiquettes d'avancée de développement de projet à chaque issue](https://about.gitlab.com/features/issueboard/)

### Récoltes d'informations auprès de praticiens de git 

*  Très souvent, une utilisation uniquement en ligne de commandes depuis l'ordinateur local (pas d'utilisation de l'interface web distante), ce qui ne permet pas de travailler à partir des "issues", ce qui rend le travail sur les branches distantes plus délicat, plus difficile;
*  Se contentent de la branche `master` sur une base de commande `git pull` et `git push`
*  Se réfèrent souvent au fichier `readme.md` de la branche `master` pour s'orienter, comprendre.